clear all
clear f_actual dx_f_actual dw_f_actual dw_dx_f_actual
clear f_new dx_f_new dw_f_new dw_dx_f_new mynet
K=[4,3,1];
L=length(K);
n=K(1);
p=rand(n,2);  %only dimension is important
t=rand(1,2);  %only dimension is important  

mynet = newff(p,t,K(2));
mynet.outputs{2}.processfcns{1,2}='removeconstantrows';
mynet.IW{1,1}=ones(K(2),K(1)); % all inputweight are in {1,1}
for k=2:(L-1)
    mynet.LW{k,k-1}=ones(K(k+1),K(k));
end
    
for k=1:(L-1) 
    mynet.b{k,1}=zeros(K(k+1),1);
end
    
ww{1}=mynet.IW{1,1}';
for k=2:(L-1)
    ww{k}=mynet.LW{k,k-1}';
end


f_actual=p(:,1);
for i=1:(L-2)
    mynet.layers{i}.transferFcn='hardlim';
end
mynet.layers{L-1}.transferFcn='purelin';


y=sim(mynet,f_actual);

for j=1:n
    for r=1:n
        if (j==r)
            dx_f_actual(j,r)=1;
        else
            dx_f_actual(j,r)=0;
        end
    end
end

for r=1:n
    for l=1:(L-1)
        dw_f_actual{l}{r}=zeros(K(l),K(l+1))    
    end
end

for j=1:n
    for r=1:n
        for l=1:(L-1)
            dw_dx_f_actual{j}{l}{r}=zeros(K(l),K(l+1)) ;   
        end
    end
end


for l=1:(L-2)
    [f_new , dx_f_new , dw_f_new , dw_dx_f_new]=update_all(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , ww{l} , K,l, ...
                                                            @phii,@d_phii,@dd_phii);
    f_actual=f_new';
    dx_f_actual=dx_f_new;
    dw_f_actual=dw_f_new;
    dw_dx_f_actual=dw_dx_f_new;
    clear f_new dx_f_new dw_f_new dw_dx_f_new 
end

[f_new , dx_f_new , dw_f_new , dw_dx_f_new]=update_all(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , ww{L-1} , K,(L-1), ...
                                                            @phii_last,@d_phii_last,@dd_phii_last);


yy=f_new;
diff=y-yy;
disp('finished');



