function stat=compute_statistics(y_train,y_test,tt_train,tt_test)
dim_output=size(tt_train,1);
n_train=size(tt_train,2);
n_test=size(tt_test,2);
                stat.err_abs_train = y_train - tt_train;
        stat.err_abs_test = y_test - tt_test;
        stat.err_rel_train = (y_train - tt_train)./ tt_train;
        stat.err_rel_test= (y_test - tt_test) ./ tt_test;
    for j=1:dim_output

        stat.err_abs_train_average(j) = mean(abs(stat.err_abs_train(j,:) ));
        stat.err_abs_test_average(j) = mean(abs(stat.err_abs_test(j,:)));
        stat.err_rel_train_average(j) = mean(abs(stat.err_rel_train(j,:)));
        stat.err_rel_test_average(j) = mean(abs(stat.err_rel_test(j,:)));
        stat.err_mse_train(j) = sum(stat.err_abs_train(j,:).^2)/n_train;
        stat.err_mse_test(j) = sum(stat.err_abs_test(j,:).^2)/n_test;

    stat.err_rel_train_average_mean = mean(stat.err_abs_train_average);
    stat.err_rel_test_average_mean = mean(stat.err_abs_test_average);
    stat.err_rel_train_average_min = min(stat.err_abs_train_average);
    stat.err_rel_test_average_min = min(stat.err_abs_test_average);
    stat.err_rel_train_average_max = max(stat.err_abs_train_average);
    stat.err_rel_test_average_max = max(stat.err_abs_test_average);

    end