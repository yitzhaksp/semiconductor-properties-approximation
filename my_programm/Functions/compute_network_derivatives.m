% computes network output f and the derivatives df/dw, df/dx, d(df/dx)/dw
%for the meaning of the arguments see
%"compute_network.m"

function [f , dx_f , dw_f , dw_dx_f]=compute_network_derivatives(x,weights,network_par) 

K=network_par.K;
L=length(K);
input_dim=K(1);
transfer_func=network_par.transfer_func;
last_transfer_func=network_par.last_transfer_func;
w=weights.w;
b=weights.b;

% set starting values for f,dx_f,dw_f and dw_dx_f
%-------------------------------------------
this_layer.f=x;

this_layer.dx_f=eye(input_dim);

if (nargout>2)
    for r=1:input_dim
        for l=1:(L-1)
            this_layer.dw_f.dw{r}{l}=zeros(K(l),K(l+1));
            this_layer.dw_f.db{r}{l}=zeros(K(l+1),1);
        end
    end
end

if (nargout>3)
    for j=1:input_dim
        for r=1:input_dim
            for l=1:(L-1)
                this_layer.dw_dx_f.dw{j,r}{l}=zeros(K(l),K(l+1)) ;
                this_layer.dw_dx_f.db{j,r}{l}=zeros(K(l+1),1) ;
            end
        end
    end
end

this_layer.num=1;
% end set starting values for f,dx_f,dw_f and dw_dx_f
%-------------------------------------------

% forward-propagate f,dx_f,dw_f and dw_dx_f
for l=1:(L-2)
    this_layer.num=l;
    f_new =  update_f(this_layer,w{l},b{l}, transfer_func);    
    if (nargout>1)
        dx_f_new =  update_dx_f(this_layer, w{l},b{l}, transfer_func,input_dim);
    end
    if (nargout>2)
        dw_f_new =  update_dw_f(this_layer, w{l},b{l}, transfer_func,K);
    end
    if (nargout>3)
        dw_dx_f_new =  update_dw_dx_f(this_layer, w{l},b{l}, transfer_func,K);
    end

    this_layer.f=f_new';    
    if (nargin>1)
        this_layer.dx_f=dx_f_new;
    end    
    if (nargout>2)
        this_layer.dw_f=dw_f_new;
    end
    if (nargout>3)
        this_layer.dw_dx_f=dw_dx_f_new;
    end
    clear f_new dx_f_new dw_f_new dw_dx_f_new 
end

%last step
% difference between this and loop before is only the transfer_func
this_layer.num=L-1;
f =  update_f(this_layer, w{L-1},b{L-1}, last_transfer_func);
if (nargout>1)
        dx_f =  update_dx_f(this_layer, w{L-1},b{L-1}, last_transfer_func,input_dim);
end
if (nargout>2)
    dw_f =  update_dw_f(this_layer, w{L-1},b{L-1}, last_transfer_func,K);
end
if (nargout>3)
    dw_dx_f =  update_dw_dx_f(this_layer, w{L-1},b{L-1}, last_transfer_func,K);
end

           