function v_sub=random_subvector(v,n)
if length(v)<n
    disp('n larger than size of vector');
    v_sub=0;
else
    rand_num=length(v)*rand(n,1);
    rand_num=ceil(rand_num);
    for i=1:n
        v_sub(i)=v(rand_num(i));
    end
end