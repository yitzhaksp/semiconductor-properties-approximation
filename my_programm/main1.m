N=50;
n_neurons=2;
%p=rand(2,N);
for i=1:N
    t(i)=somefunction( p(1,i),p(2,i) );
end
mynet = newff(p,t,n_neurons);
mynet.trainParam.epochs=300;
[mynet,tr] = train(mynet,p,t);
for i=1:N
    y(i)=sim(mynet,p(:,i));
end
err=abs( (t-y)./t );
err_mean = mean(err);
x=[0.5;0.5];
z=sim(mynet,x);
z_real=somefunction( x(1),x(2) );
err_z=abs( (z - z_real) / z_real );
