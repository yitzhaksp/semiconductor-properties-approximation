function [f_new , dx_f_new , dw_f_new , dw_dx_f_new]=update_all(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , w)
    f_new =  update_f(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , w);
    dx_f_new =  update_dx_f(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , w);
    dw_f_new =  update_dw_f(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , w);
    dw_dx_f_new =  update_dw_dx_f(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , w);

return

function f_new = update_f(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , w);
    K_actual= ..(size(w..));
    K_new= ..(size(w..)); 
    for k=1:K_new
        f_new(k) = phi( w(:,k)*f_actual );
    end
return


function dx_f_new = update_dx_f(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , w);
    for k=1:K_new
        for j=1:n
            dx_f_new(k,j) = d_phi( w(:,k)*f_actual ) * (w(:,k)*dx_f_actual(:,j)) ;
        end
    end
    
return
 

function dw_f_new = update_dw_f(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , w);
    K_actual= ..(size(w..));
    K_new= ..(size(w..));
    for k=1:K_new
        Term_1=d_phi(w(:,k)*f_actual);
        Term_2=0;
        for r=1:K_actual
            Term_2 = Term_2 + I_lrk*f_actual(r) +w_lrk*dw_f_actual(..,r);
        end
        dw_f_new(k)=Term_1 * Term_2 ;
    end
return
    
function dw_dx_f_new = update_dw_dx_f(f_actual , dx_f_actual , dw_f_actual , dw_dx_f_actual , w);
    K_actual= ..(size(w..));
    K_new= ..(size(w..));
    for j=1:n
        value_111 = dd_phi(w(:,k)*f_actual);

        value_112 = 0;
        for r=1:K_actual
            value_112= value_112 + I_lrk*f_actual(r) +w_lrk*dw_f_actual(..,r);
        end


        value_11=value_111 * value_112;
        value_12=w(:,k)*dx_f_actual(:,j)
        value_21= d_phi( w(:,k)*f_actual );    

        value_22=0
        for r=1:K_actual
            value_22= value_22 + I_lrk*dx_f_actual(r,j) +w_lrk*dw_dx_f_actual(..,j,r);
        end


        dw_dx_f_new(..,j)= (value_11 * value_12) + (value_21 * value_22) ;
    end
    
    
 
return