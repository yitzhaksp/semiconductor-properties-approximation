clear p_train tt_train
clear p_test tt_test
clear err_abs_train err_abs_test
clear err_rel_train err_rel_test
clear y_train y_test

load data/p
load data/t

tt=t(50,:);
N=length(tt);
%n_neurons=20;   % number of neurons in first hidden layer
%N_NEURONS=[5 5 5];
%N_NEURONS=[4 4 4];
%N_NEURONS=[4 4 ];
%N_NEURONS=[40 ];
%N_NEURONS=[60 ];
%N_NEURONS=[6 6 6 ];  
% N_NEURONS=[7 7 7] 
% N_NEURONS=[8 8 8] 
% N_NEURONS=[9 9 9] 
% N_NEURONS=10*[1 1 1] 
% N_NEURONS=15*[1 1 1] 
% N_NEURONS=20*[1 1 1] 
 N_NEURONS=12*[1 1 1 1] %%%%%%% the best
%N_NEURONS=15*[1 1 1 ] 
%N_NEURONS=[40 5]

%N_NEURONS=15*[1 1 1 1]; 
%N_NEURONS=30*[1 1 1]; 
%N_NEURONS=[20 10]; 
%N_NEURONS=[20 5]; 

%N_NEURONS=[9 9 9]; 
%N_NEURONS=[6 10];  
%N_NEURONS=[7 6  6 ]; 
%N_NEURONS=[5 5 5 5];

Portion_training=0.7; % Portion of training samples
training_epochs=20; 



P=1-Portion_training;       % Portion of validation samples

% separate training samples from validation samples

[Train,Test]=crossvalind('HoldOut',N,P);
k_train=1;
k_test=1;
for i=1:N
    if (Train(i)==1)
        p_train(:,k_train)=p(:,i);
        tt_train(k_train)=tt(i);
        k_train=k_train+1;
    else
        p_test(:,k_test)=p(:,i);
        tt_test(k_test)=tt(i);
        k_test=k_test+1 ;
    end
  
end




% create, train and apply neural network

%mynet = newff(p_train,tt_train,n_neurons);
mynet = newff(p_train,tt_train,N_NEURONS);
%mynet = newff(p_train,tt_train,[n_neurons,n_neurons/4]);

mynet.trainParam.epochs=training_epochs;
[mynet,tr] = train(mynet,p_train,tt_train);
y_train=sim(mynet,p_train);
y_test=sim(mynet,p_test);




% compute errors and statistics

err_abs_train = y_train - tt_train;
err_abs_test = y_test - tt_test;
err_rel_train = (y_train - tt_train)./ tt_train;
err_rel_test = (y_test - tt_test) ./ tt_test;

err_abs_train_average = mean(abs(err_abs_train));
err_abs_test_average = mean(abs(err_abs_test));
err_rel_train_average = mean(abs(err_rel_train));
err_rel_test_average = mean(abs(err_rel_test));

err_abs_train_variance = var(err_abs_train);
err_abs_test_variance = var(err_abs_test);
err_rel_train_variance = var(err_rel_train);
err_rel_test_variance = var(err_rel_test);


[tmp,ind]=sort(tt_test);
figure;
subplot(211); plot([tt_test(ind)'  y_test(ind)']);title('Sorted test data')
subplot(212); plot([tt_test(ind)' - y_test(ind)']);
title(sprintf('Residual:  root-mean-square-error = %.1e',sqrt(mean((y_test - tt_test).^2))));
