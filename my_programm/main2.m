clear all

addpath(genpath('.'));
load data/FromMichaelKlotz ;    
%load data/Indices_20000 ;
    

p=x1.measRefl; % input data
t=x1.h; % target values

dim_output=size(t,1);
tt=t(1:dim_output,:);

% simulation parameters

N=length(tt); % total number of examples for training and validation
train_ratio=0.8; % number of examples for training
training_epochs=10; % maximal number of steps in a training session
n_sim=2; % number of training sessions with different initial weights each time
new_perm_FLAG=true; % create new permutation for separation of training and testing data?

n_train = floor(N*train_ratio);
n_test = N-n_train;



% neural network parameters

n_neurons=[5 5]; % network structure
transfer_fcn='tansig';
net_trainRatio=0.7;
net_valRatio=0.3;

net_testRatio=1-net_trainRatio-net_valRatio;
n_layers=length(n_neurons);



% separate training data from testing data

if (new_perm_FLAG==true)
    Indices=randperm(N); 
end


for i=1:N
    p_perm(:,i)=p(:,Indices(i));
    tt_perm(:,i)=tt(:,Indices(i));
end

p_train=p_perm(:,1:n_train);
p_test=p_perm(:,(n_train+1):N);
tt_train=tt_perm(:,1:n_train);
tt_test=tt_perm(:,(n_train+1):N);

    

% create neural network

mynet = newff(p_train,tt_train,n_neurons);
mynet.trainParam.epochs=training_epochs;
mynet.divideFcn='divideblock';
mynet.divideParam.trainRatio=net_trainRatio;
mynet.divideParam.valRatio=net_valRatio;
mynet.divideParam.testRatio=net_testRatio;

for i=1:n_layers
    mynet.layers{i}.transferFcn=transfer_fcn
end



%train network with individual initial weights in each loop

for i=1:n_sim

    % train and apply neural network

    mynet=init(mynet);
    [mynet,tr] = train(mynet,p_train,tt_train);
    IW(i,:,:)=mynet.IW;
    LW(i,:,:)=mynet.LW;
    b(i,:,:)=mynet.b;
    y_train=sim(mynet,p_train);
    y_test=sim(mynet,p_test);
    stat=compute_statistics(y_train,y_test,tt_train,tt_test);

end

    
%err_mse_train = err_mse_train';
%err_mse_test = err_mse_test';
    
%sum_square_y_train=sum_square_y_train';
%sum_square_y_test=sum_square_y_test';

