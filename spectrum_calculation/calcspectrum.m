%Here is a function for generating the spectrum of a function of a set of 6
%input parameters (stackparams).
%The range of stackparams values where the interpolant that approximate the
%real spectrum must be constructed is (approximately)

%L1H = stackparams(1);  %mat_u78_i + mat_air: 1170 to 1390 angstrom (height of
%upper layer)

%L1CD = stackparams(2);  %mat_u78_i + mat_air: from 0 to 50 default 10
%(percent of upper layer that is filled with material mat_u78_i (the rest is
%filled with air)

%L2H = stackparams(3); %mat_v89be_i + mat_air : 9500 to 11100 angstrom
%(height of second layer from top)

%L2CD = stackparams(4); %mat_v89be_i + mat_air from 0 to 50 default 10
%(percent of second layer from top that is filled with material mat_v89be_i (the
%rest is filled with air)

%L3H = stackparams(5); %mat_v89m: 720 to 880 angstrom (height of third
%layer from top)

%L4H = stackparams(6); %mat_ox: 700 to 1900 angstrom (height of fourth
%layer from top)

%targetwl parameter: the vector of wavelengths where the spectrum need to
%be evaluated
function spec=calcspectrum(stackparams,targetwl,bPlotSpec)
if(nargin<1)
    stackparams = [ 1200 10 10000 10 750 1000];
end
if(nargin<2)
    target_wl = [ 240 : 1.0 : 900];
end
if(nargin<3)
    bPlotSpec=1;
end

L1H = stackparams(1)/10;  %mat_u78_i + mat_air: 1170 to 1390
L1CD = stackparams(2);  %mat_u78_i + mat_air: from 0 to 50 default 10
L2H = stackparams(3)/10; %mat_v89be_i + mat_air : 9500 to 11100
L2CD = stackparams(4); %mat_v89be_i + mat_air from 0 to 50 default 10
L3H = stackparams(5)/10; %mat_v89m: 720 to 880
L4H = stackparams(6)/10; %mat_ox: 700 to 1900

mat_si = load_nkf('Si_UV_CALIBRATION.nkf');
mat_ox = load_nkf('SiO2_IR1_2.nkf');
mat_air = load_nkf('Air-CHY-Tbl.nkf');
mat_u78 = load_nkf('U78Y_25L_Nitride_MSMC_Pole_051509_2_Tbl.nkf');
mat_v89be = load_nkf('V89Z_BE_Thick_PSG_Poles_093010_4_Tbl.nkf');
mat_v89m = load_nkf('V89Z_55_Metal_Combo_Pole_050510_5_KV6.nkf');


mat_u78_i = interpolate_mat(mat_u78,target_wl);
mat_v89be_i = interpolate_mat(mat_v89be,target_wl);
mat_air_i = interpolate_mat(mat_air,target_wl);


eps_L1 = EffectiveEps(L1CD./100,mat_u78_i,1-L1CD./100,mat_air_i);
eps_L2 = EffectiveEps(L2CD./100,mat_v89be_i,1-L2CD./100,mat_air_i);
mat_L1 = getnandk(eps_L1.epsTE);
mat_L1.l = target_wl;
mat_L2 = getnandk(eps_L2.epsTE);
mat_L2.l = target_wl;



matrs = { mat_air mat_L1 mat_L2  mat_v89m mat_ox mat_si }; %materials of layers from top to bottom (first and last layer are ambient and substrate (with infinite height)
appStack = MakeSolidStack(matrs,target_wl);
appStack.d(1+1) = L1H;
appStack.d(2+1) = L2H;
appStack.d(3+1) = L3H;
appStack.d(4+1) = L4H;

appStack.ParMin = [1170 9500 720 700];
appStack.ParMax = [1390 111000 880  1900];

[Rtest ER]= CalcSolidSpectrum(appStack);

spec = abs(Rtest);
spec = spec.*spec;
if(bPlotSpec)
    plot(target_wl,spec);
end


%********************************************************************
%first material is ambient, last material is substrate
function [stack] = MakeSolidStack(materialsTopToBottom,target_wl)

nlayers = length(materialsTopToBottom);
nwl = length(target_wl);
stack.materialsTopToBottom = materialsTopToBottom;
stack.d = zeros(nlayers-1,1); %exclude substrate
stack.theta =0; %default: normal incidence
stack.pol =0; % default: pol= TE;
stack.lam = target_wl;
stack.NiK = zeros(nlayers,nwl);

for i = 1:nlayers;
    matr = materialsTopToBottom{i};
    matri = interpolate_mat(matr,target_wl);
    stack.NiK(i,:) = matri.n + 1.0i*matri.k;
end

return;
%********************************************************************8
function [mati] = interpolate_mat(mat,target_wl)

srcwl = mat.l;
mati.l = target_wl;
mati.n = interp1(srcwl,mat.n,target_wl,'spline');
mati.k = interp1(srcwl,mat.k,target_wl,'spline');
return;

%********************************************************************8
function [R, ER] = CalcSolidSpectrum(solidStack)

d = solidStack.d;
theta = solidStack.theta;
pol = solidStack.pol;
lam = solidStack.lam;
NiK = solidStack.NiK;
[R,ER]=solid_spec(d,theta,pol,lam,NiK);

% Title: Algorithm for the calculation of the reflectance
%        of electromagnetic wave off a stratified media.
%
% Synopsis: [R,ER]=solid_spec(d,theta,pol,lam,NiK)
%
%
%
%
% Input:  d - Column vectror of layers thicknesses. d(1)=0, d(2)=thickness
%              of layer below ambient,...d(L+1)=thickness of layer above
%              substrate where L is number of layers excluding substrate and ambient.
%              The units of d should be the same as that of the lam vector.
%         theta - Incidence angle in Radians.
%         pol - Polarization flag: pol=0 is for TE while pol=1 is for TM.
%         lam - Row vector of wavelengths the same units as d and size M.
%         NiK - n+ik matrix with size (L+2)xM such that NiK(ii,jj) is the value
%         of n+ik for layer ii at wavelength lam(jj).
%
% Output: R - A raw vector of size M such that R(jj) is the amplitude and
%             phase of the reflected wave at the upper interface.
%         ER - The reflectanc to intensity defined by ???abs(R).^2/cos(theta)???

%   Author: S. Gov 11 Nov. 2000
%   Copyright (c) NOVA Measuring Instruments Ltd.


function [R,ER]=solid_spec(d,theta,pol,lam,NiK),


switch pol
    
    case 0 , % TE polarization
        
        
        d(1)=0 ; % Set d(1) to 0 , just in case
        nsint=NiK(1,:)*sin(theta);  % (n+ik)*sin(theta_substrate)
        max_j=length(d)+1;
        sint2=nsint./NiK(max_j,:);  % sin(theta_substrate)
        ncost2=NiK(max_j,:).*sqrt(1-sint2.^2);    % (n+ik)*cos(theta_substrate)
        r=zeros(size(lam));
        
        for j=(max_j-1):(-1):1, % running over stack layers from bottom
            
            sint1=nsint./NiK(j,:);             % sin(theta_layer_j)
            ncost1=NiK(j,:).*sqrt(1-sint1.^2); % (n+ik)*cos(theta_layer_j)
            
            % Evaluate reflectance at lower interface of layer j
            a1=ncost1.*(1+r);
            a2=ncost2.*(1-r);
            r=(a1-a2)./(a1+a2);
            
            % Propagate reflectance to upper interface
            r=r.*exp(2*sqrt(-1)*2*pi.*(d(j)./lam).*ncost1);
            
            ncost2=ncost1; % Change name for next (upper) layer
        end
        
        R=r;
        ER=abs(R).^2;
        
        
        
    case 1 , % TM polarization
        
        d(1)=0 ; % Set d(1) to 0 , just in case
        nsint=NiK(1,:)*sin(theta);  % (n+ik)*sin(theta_substrate)
        max_j=length(d)+1;
        sint2=nsint./NiK(max_j,:);  % sin(theta_substrate)
        cost2on=sqrt(1-sint2.^2)./NiK(max_j,:);    % cos(theta_substrate)/(n+ik)
        r=zeros(size(lam));
        
        for j=(max_j-1):(-1):1, % running over stack layers from bottom
            
            sint1=nsint./NiK(j,:);             % sin(theta_layer_j)
            cost1=sqrt(1-sint1.^2);            % cos(theta_layer_j)
            cost1on=cost1./NiK(j,:);           % cos(theta_layer_j)/(n+ik)
            
            % Evaluate reflectance at lower interface of layer j
            a1=cost1on.*(1+r);
            a2=cost2on.*(1-r);
            r=(a1-a2)./(a1+a2);
            
            % Propagate reflectance to upper interface
            r=r.*exp(2*sqrt(-1)*2*pi.*(d(j)./lam).*cost1.*NiK(j,:));
            
            cost2on=cost1on; % Change name for next (upper) layer
        end
        
        R=r;
        ER=abs(R).^2;
        
end % Of switch between polarizations


return

%********************************************************************8
function [mat]=load_nkf(fname)
%function that load materials specs as specified in 'name.nkf' files
%return error<>0 if some error
[jnkpath,matname,jnkext]=fileparts(fname);
if(~strcmp(jnkext,'.nkf'))
    %nothing to do, if fname is not a nkf file
    error=0;
    return;
end
error=1;
try
    tmp=load(fname);
catch
    uiwait(errordlg(['Error while trying to load material n&k file: ' fname]));
    return;
end
mat.l = tmp(:,1);
mat.n = tmp(:,2);
mat.k = tmp(:,3);
return;




%********************************************************************8
function SaveEffEpsAsNKF(wlvec, effeps,name);

SaveEpsAsNKF(wlvec,effeps.epsTE,[name '_TE.nkf']);
SaveEpsAsNKF(wlvec,effeps.epsTM,[name '_TM.nkf']);
%********************************************************************8
function SaveEpsAsNKF(wlvec, mateps,name);

nandk = getnandk(mateps);

SaveNKF(wlvec,nandk.n,nandk.k,name);
%********************************************************************8
function SaveMatAsNKF(mat2save,name);
hf = fopen(name, 'w');
wlvec = mat2save.l;
n = mat2save.n;
k  = mat2save.k;
nwl = length(wlvec);

for i = 1: nwl
    count=fprintf(hf,'%4.5f %3.5f %3.5f \r\n', wlvec(i),n(i),k(i));
end

fclose(hf);
%********************************************************************8
function SaveNKF(wlvec,n,k,name);
hf = fopen(name, 'w');

nwl = length(wlvec);

for i = 1: nwl
    count=fprintf(hf,'%4.5f %3.5f %3.5f \r\n', wlvec(i),n(i),k(i));
end

fclose(hf);
%********************************************************************8
function [effeps] =EffectiveEps( w1, mat1, w2, mat2)
mateps1 = getmateps(mat1);
mateps2 = getmateps(mat2);

effeps.epsTE = EffectiveEpsTE( w1, mateps1, w2, mateps2);
effeps.epsTM = EffectiveEpsTM( w1, mateps1, w2, mateps2);

%********************************************************************8
function [effeps] =EffectiveEpsTE( w1, eps1, w2, eps2)

effeps = w1*eps1 + w2*eps2;
%********************************************************************8
function [effeps] =EffectiveEpsTM( w1, eps1, w2, eps2)

inveffeps = w1./eps1 + w2./eps2;
effeps = 1./inveffeps;



%********************************************************************8
function [mateps] = getmateps(mat)

mateps= (mat.n + 1.0i*mat.k).^2;


%********************************************************************8
function [nandk] = getnandk(mateps)

sqrteps = sqrt(mateps);
nandk.n = abs(real(sqrteps));
nandk.k = abs(imag(sqrteps));

